# Локальная сборка докера

```
docker build \ 
    ./ \ # - текущая папка с Dockerfile
    -t test-ge:v0.0.1 # - имя:тег, с которыми соберется образ
```

Пример:

```
docker build ./ -t test-ge:v0.0.1
```

# Локальный запуск докера
```
docker run \
    -v <полный путь к папке с файлом env.*>:/conf \
    -e ENVIRONMENT=test \
    test-ge:v0.0.1 (имя:тег docker-образа из предыдущего шага)
```
Пример:
```
docker run -v C:\Users\username\projects\market-bot\api-reader\conf:/conf -e ENVIRONMENT=test test-ge:v0.0.1
```

# Файлы конфигураций

```
/conf/env.*
```
