package config

import (
	"os"
	"fmt"

	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
)

type config struct {
	Url    string `env:"API_URL"`
	Path   string `env:"API_PATH"`
	Https  bool   `env:"HTTPS" envDefault:"false"`
	Port   int    `env:"PORT" envDefault:"8080"`
	IsTest bool   `env:"TEST" envDefault:"false"`
}

func GetConfig() (cfg config) {

	environment := os.Getenv("ENVIRONMENT")

	err := godotenv.Load("/conf/.env." + environment)
	if err != nil {
		fmt.Printf("Error loading /conf/.env.%s file from \n", environment)
		os.Exit(1)
	}

	cfg = config{}

	if err := env.Parse(&cfg); err != nil {
		fmt.Printf("%+v\n", err)
		os.Exit(1)
	}

	return
}
